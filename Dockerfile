FROM golang:1.13.0-alpine3.10

RUN apk add --no-cache ca-certificates \
    dpkg \
    gcc \
    git \
    musl-dev \
    tzdata \
    curl \
    bash

RUN cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo "America/Sao_Paulo" > /etc/timezone && \
    date && \
    apk del tzdata

ENV GOPATH /go
ENV GO111MODULE on
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "${GOPATH}/src" "${GOPATH}/bin"

RUN curl https://glide.sh/get | sh

EXPOSE 9000

WORKDIR ${GOPATH}
