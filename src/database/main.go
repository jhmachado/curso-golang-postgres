package database

import (
	"os"
	"log"

	pg "github.com/go-pg/pg"
)

func Connect() {
	opts := &pg.Options{
		User: "postgres",
		Password: "secret",
		Addr: "database:5432",
	}

	db := pg.Connect(opts)

	if db == nil {
		log.Printf("Unable to connect to the database")
		os.Exit(100)
	}

	log.Printf("Connected to the database")

	closeErr := db.Close()

	if closeErr != nil {
		log.Printf("Error while closing the connection. Reason: %v", closeErr)
		os.Exit(100)
	}

	log.Printf("Connection closed")
	return
}
