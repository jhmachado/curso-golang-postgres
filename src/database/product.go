package database

import (
	"time"
)

type Feature struct {
	ID 		     int	 	`sql:"id,pk"`
	Name 		 string  	`sql:"name"`
	Description  string  	`sql:"description"`
	Imp 		 int        `sql:"imp"`
} 

type Product struct {
	tableName 	 struct{}  	`sql:"product_collection"`
	ID 		     int	    `sql:"id,pk"`
	name	     string     `sql:"name,unique"`
	Description  string     `sql:"description"`
	Image        string     `sql:"image"`
	Price        string     `sql:"price"`
	Features     []Feature  `sql:"features"`
	CreatedAt 	 time.Time  `sql:"created_at"`
	UpdatedAt 	 time.Time  `sql:"updated_at"`
	IsActive  	 bool	    `sql:"is_active"`
}
